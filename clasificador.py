import sys


def es_correo_electronico(string):
    try:
        if "@" in string:
            separar = string.split("@")
            if separar[0] != "" and "." in separar[1]:
                return True
        else:
            return False
    except:
        return False

def es_entero(string):
    try:
        int(string)
        return True
    except ValueError:
     return False

def es_real(entrada):
    try:
        float(entrada)
        return True
    except ValueError:
        return False


def evaluar_entrada(string):
    if string == "":
        respuesta = None
    elif es_correo_electronico(string):
        respuesta = "Es un correo electrónico."
    elif es_entero(string):
        respuesta = "Es un entero."
    elif es_real(string):
        respuesta = "Es un número real."
    else:
        respuesta = "No es ni un correo, ni un entero, ni un número real."
    return respuesta

def main():
    if len(sys.argv) < 2:
        sys.exit("Error: se espera al menos un argumento")
    string = sys.argv[1]
    resultado = evaluar_entrada(string)
    print(resultado)

if __name__ == '__main__':
    main()
